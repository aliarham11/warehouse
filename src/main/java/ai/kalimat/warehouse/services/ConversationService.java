package ai.kalimat.warehouse.services;

import java.util.List;
import java.util.Map;

import ai.kalimat.warehouse.models.Conversation;

public interface ConversationService {
	
	Map<String, Conversation> storeData(Conversation conversation);
	
	Map<String, Conversation> fetchById(Integer id);
	
	Map<String, List<Conversation>> fetchAll();
	
	Map<String, String> removeById(Conversation conversation);
	
}
