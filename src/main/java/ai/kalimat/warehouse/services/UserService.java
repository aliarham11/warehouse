package ai.kalimat.warehouse.services;

import java.util.List;
import java.util.Map;

import ai.kalimat.warehouse.models.User;

public interface UserService {

	Map<String, User> storeData(User user);
	
	Map<String, User> fetchById(Integer id);
	
	Map<String, List<User>> fetchAll();
	
	Map<String, String> removeById(User user);
	
}
