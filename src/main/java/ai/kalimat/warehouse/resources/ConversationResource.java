package ai.kalimat.warehouse.resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ai.kalimat.warehouse.containers.ServiceContainer;
import ai.kalimat.warehouse.models.Conversation;
import ai.kalimat.warehouse.services.ConversationService;

@Path("conversations")
public class ConversationResource {

	private ConversationService conversationService = ServiceContainer.getConversationServiceInstance();
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveOrUpdate(Conversation conversation){
		return Response.ok(conversationService.storeData(conversation)).build();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") Integer id){
		return Response.ok(conversationService.fetchById(id)).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(){
		return Response.ok(conversationService.fetchAll()).build();
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteById(Conversation conversation){
		return Response.ok(conversationService.removeById(conversation)).build();
	}

}
