package ai.kalimat.warehouse.resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ai.kalimat.warehouse.containers.ServiceContainer;
import ai.kalimat.warehouse.models.User;
import ai.kalimat.warehouse.services.UserService;

@Path("users")
public class UserResource {
	
	private UserService userService = ServiceContainer.getUserServiceInstance();
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveOrUpdate(User user){
		return Response.ok(userService.storeData(user)).build();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") Integer id){
		return Response.ok(userService.fetchById(id)).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(){
		return Response.ok(userService.fetchAll()).build();
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteById(User user){
		return Response.ok(userService.removeById(user)).build();
	}

}
