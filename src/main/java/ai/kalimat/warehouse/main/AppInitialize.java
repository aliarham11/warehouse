package ai.kalimat.warehouse.main;

import ai.kalimat.warehouse.injectors.Injector;
import ai.kalimat.warehouse.repos.ConversationRepo;
import ai.kalimat.warehouse.repos.UserRepo;
import ai.kalimat.warehouse.server.ApiServer;
import ai.kalimat.warehouse.storages.MemStore;
import ai.kalimat.warehouse.storages.Store;



public class AppInitialize {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		System.out.println("---- App started ----");

		if (args[0].equals("initFromCode")) {
			Store store = new MemStore();

			UserRepo userRepo = new UserRepo(store);
			ConversationRepo conversationRepo = new ConversationRepo(store);
			ApiServer server = new ApiServer(userRepo, conversationRepo);
			server.start(5000);
		} else {
			Injector injector = new Injector();
			injector.loadConfig(args[0]);
		}

	}

}
