package ai.kalimat.warehouse.repos;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import ai.kalimat.warehouse.storages.Store;

public class GenericRepo {

	private Store<JSONObject> store;
	
	private JSONObject dataSchema;
	
	private final String ENTITY; 
	
	@SuppressWarnings("unchecked")
	public GenericRepo(Store<JSONObject> store, String schema){
		this.store = store;
		
		try {
			JSONParser parser = new JSONParser();
			dataSchema = (JSONObject) parser.parse(schema);
			dataSchema.put("id", "integer");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		ENTITY = dataSchema.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject save(JSONObject data){
		if(validateSchema(data)){
			data.put("isDeleted", false);
			store.saveOrUpdate(ENTITY, data, Integer.parseInt(data.get("id").toString()));
			data.remove("isDeleted");
			return data;
		}
		
		return null;
	}
	
	public JSONObject getById(Integer id){
		JSONObject result = store.getById(ENTITY, id);
		if(Boolean.parseBoolean(result.get("isDeleted").toString()) == true){
			try {
				return getDefault();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		result.remove("isDeleted");
		return result;
	}
	
	public List<JSONObject> getAll(){
		List<JSONObject> tempResult = store.getAll(ENTITY);
		List<JSONObject> result = tempResult;
		if(tempResult != null) {
			for(int i = 0; i < tempResult.size(); i++){
				if(Boolean.parseBoolean(tempResult.get(i).get("isDeleted").toString()) == true){
					result.remove(i);
				} else {
					result.get(i).remove("isDeleted");
				}
				
			}
		} else {
			try {
				return Arrays.asList(new JSONObject[] { getDefault() });
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> removeById(JSONObject data){
		Map<String, String> result = new ConcurrentHashMap<String, String>();
		data.put("isDeleted", true);
		result.put("status", store.delete(
				ENTITY, 
				data, 
				Integer.parseInt(data.get("id").toString())
			));
		return result;
	}

	@SuppressWarnings("rawtypes")
	public Boolean validateSchema(JSONObject data){
		Set keys = dataSchema.keySet();
		if(keys.size() != data.size()) return false;
	    Iterator a = keys.iterator();
	    while(a.hasNext()) {
	    	String key = (String)a.next();
	    	if(data.get(key) == null) return false;
	    }
	    
	    return true;
	}
	
	public JSONObject getDefault() throws ParseException{
		JSONParser parser = new JSONParser();
		return (JSONObject)parser.parse("{\"result\":\" no data \"}");
	}
	
	public Store<JSONObject> getStore() {
		return store;
	}
	
	
	
	
}
