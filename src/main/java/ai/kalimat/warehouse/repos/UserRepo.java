package ai.kalimat.warehouse.repos;

import java.util.List;

import ai.kalimat.warehouse.models.User;
import ai.kalimat.warehouse.storages.Store;

public class UserRepo {
		
	private Store<User> store;
	
	private final String ENTITY = User.class.getSimpleName(); 
	
	public UserRepo(Store<User> store){
		this.store = store;
	}
	
	public User save(User user){
		user.setIsDeleted(false);
		User savedUser = store.saveOrUpdate(ENTITY, user, user.getId());
		return savedUser;
	}
	
	public User getById(Integer id){
		User user = store.getById(ENTITY, id);
		if(user != null && user.getIsDeleted()){
			return null;
		}
		
		return user; 
	}
	
	public List<User> getAll(){
		List<User> users = store.getAll(ENTITY);
		List<User> tempUsers = users;
		if(users != null) {
			for(int i = 0; i < users.size(); i++){
				if(tempUsers.get(i).getIsDeleted()){
					users.remove(i);
				}
			}
		}
		
		return users;
	}
	
	public String removeById(User user){
		user.setIsDeleted(true);
		return store.delete(ENTITY, user, user.getId());
	}

	public Store<User> getStore() {
		return store;
	}

	public User getDefault() {
		return new User(
		0,
		"null",
		"null",
		"null",
		"null",
		"null",
		false
		);
	}
	
}
