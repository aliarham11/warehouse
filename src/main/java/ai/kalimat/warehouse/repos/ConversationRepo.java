package ai.kalimat.warehouse.repos;

import java.util.List;

import ai.kalimat.warehouse.models.Conversation;
import ai.kalimat.warehouse.storages.Store;

public class ConversationRepo {
	
	private Store<Conversation> store;
	
	private final String ENTITY = Conversation.class.getSimpleName(); 
	
	public ConversationRepo(Store<Conversation> store){
		this.store = store;
	}
	
	public Conversation save(Conversation conversation){
		conversation.setIsDeleted(false);
		Conversation savedConversation = store.saveOrUpdate(ENTITY, conversation, conversation.getId());
		return savedConversation;
	}
	
	public Conversation getById(Integer id){
		Conversation conversation = store.getById(ENTITY, id);
		if(conversation != null && conversation.getIsDeleted()){
			return null;
		}
		
		return conversation; 
	}
	
	public List<Conversation> getAll(){
		List<Conversation> conversations = store.getAll(ENTITY);
		List<Conversation> tempConversations = conversations;
		if(conversations != null) {
			for(int i = 0; i < conversations.size(); i++){
				if(tempConversations.get(i).getIsDeleted()){
					conversations.remove(i);
				}
			}
		}
		
		return conversations;
	}
	
	public String removeById(Conversation conversation){
		conversation.setIsDeleted(true);
		return store.delete(ENTITY, conversation, conversation.getId());
	}

	public Store<Conversation> getStore() {
		return store;
	}
	
	public Conversation getDefault() {
		return new Conversation(
		0,
		0,
		"null",
		"null",
		0,
		false
		);
	}
	
}
