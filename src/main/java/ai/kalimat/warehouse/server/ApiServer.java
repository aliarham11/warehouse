package ai.kalimat.warehouse.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import ai.kalimat.warehouse.containers.RepoContainer;
import ai.kalimat.warehouse.containers.ServiceContainer;
import ai.kalimat.warehouse.repos.ConversationRepo;
import ai.kalimat.warehouse.repos.UserRepo;
import ai.kalimat.warehouse.servicesimpl.ConversationServiceImpl;
import ai.kalimat.warehouse.servicesimpl.UserServiceImpl;

public class ApiServer {

	private ResourceConfig config;
	
	private ServletHolder servlet;
	
	private ServletContextHandler context;
	
	private Server server;
	
	public ApiServer(UserRepo userRepo, ConversationRepo conversationRepo) {
		initializeContainer(userRepo, conversationRepo);
	}
	
	public void start(Integer port){
		config = new ResourceConfig();
		config.packages("ai.kalimat.warehouse.resources");
		config.register(JacksonFeature.class);
	
		
		servlet = new ServletHolder(new ServletContainer(config));
		server = new Server(port);
		context = new ServletContextHandler(server, "/*");
		context.addServlet(servlet, "/*");
		
		try {
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			server.destroy();
		}
		
	}
	
	@SuppressWarnings("unused")
	public void initializeContainer(UserRepo userRepo, ConversationRepo conversationRepo){
		RepoContainer repoContainer = new RepoContainer(userRepo, conversationRepo);
		ServiceContainer serviceContainer = new ServiceContainer(
				new UserServiceImpl(),
				new ConversationServiceImpl());
	}
	
}
