package ai.kalimat.warehouse.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.process.Inflector;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;
import org.glassfish.jersey.servlet.ServletContainer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import ai.kalimat.warehouse.containers.ObjectContainer;
import ai.kalimat.warehouse.repos.GenericRepo;

public class ApiServerGeneric {

	private ResourceConfig config;

	private ServletHolder servlet;

	private ServletContextHandler context;

	private Server server;

	private Set<Resource> resources;

	private final String ROOT_PATH = "generic";

	public ApiServerGeneric(String[] path, GenericRepo[] genericRepo) {
		resources = new HashSet<Resource>();
		buildCRUDResource(path, genericRepo);
	}

	public ApiServerGeneric(String paths, String repoNames) {
		resources = new HashSet<Resource>();
		String[] splitedPath = paths.split(";");
		String[] splitedRepoName = repoNames.split(";");
		GenericRepo[] genericRepo = new GenericRepo[splitedRepoName.length];

		for (int i = 0; i < splitedRepoName.length; i++) {
			genericRepo[i] = (GenericRepo) ObjectContainer
					.getObject(splitedRepoName[i]);
		}

		buildCRUDResource(splitedPath, genericRepo);
	}

	public void start(Integer port) {
		config = new ResourceConfig();
		config.packages("ai.kalimat.warehouse.resources");
		config.register(JacksonFeature.class);
		config.registerResources(resources);
		servlet = new ServletHolder(new ServletContainer(config));
		server = new Server(port);
		context = new ServletContextHandler(server, "/*");
		context.addServlet(servlet, "/*");

		try {
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			server.destroy();
		}

	}

	public void buildCRUDResource(String[] path, GenericRepo[] genericRepo) {
		Resource.Builder resourceBuilder;
		ResourceMethod.Builder methodBuilder;
		Resource resource;
		for (int i = 0; i < path.length; i++) {
			GenericRepo temp = genericRepo[i];

			// Build GET /path resource
			resourceBuilder = Resource.builder();
			resourceBuilder.path(ROOT_PATH + path[i]);
			methodBuilder = resourceBuilder.addMethod("GET");
			methodBuilder.produces(MediaType.APPLICATION_JSON).handledBy(
					new Inflector<ContainerRequestContext, Response>() {

						@Override
						public Response apply(
								ContainerRequestContext containerRequestContext) {

							return Response.ok(temp.getAll()).build();
						}
					});

			resource = resourceBuilder.build();
			resources.add(resource);

			// Build GET /path/{id} resource
			resourceBuilder = Resource.builder();
			resourceBuilder.path(ROOT_PATH + path[i] + "/{id}");
			methodBuilder = resourceBuilder.addMethod("GET");
			methodBuilder.produces(MediaType.APPLICATION_JSON).handledBy(
					new Inflector<ContainerRequestContext, Response>() {

						@Override
						public Response apply(
								ContainerRequestContext containerRequestContext) {
							Integer id = Integer
									.parseInt(containerRequestContext
											.getUriInfo().getPathParameters()
											.get("id").get(0));
							return Response.ok(temp.getById(id)).build();
						}
					});

			resource = resourceBuilder.build();
			resources.add(resource);

			// Build POST /path resource
			resourceBuilder = Resource.builder();
			resourceBuilder.path(ROOT_PATH + path[i]);
			methodBuilder = resourceBuilder.addMethod("POST");
			methodBuilder.produces(MediaType.APPLICATION_JSON).handledBy(
					new Inflector<ContainerRequestContext, Response>() {

						@Override
						public Response apply(ContainerRequestContext request) {
							JSONObject param = convertFromInputStream(request
									.getEntityStream());
							return Response.ok(temp.save(param)).build();
						}
					});

			resource = resourceBuilder.build();
			resources.add(resource);

			// Build DELETE /path resource
			resourceBuilder = Resource.builder();
			resourceBuilder.path(ROOT_PATH + path[i]);
			methodBuilder = resourceBuilder.addMethod("DELETE");
			methodBuilder.produces(MediaType.APPLICATION_JSON).handledBy(
					new Inflector<ContainerRequestContext, Response>() {

						@Override
						public Response apply(ContainerRequestContext request) {
							JSONObject param = convertFromInputStream(request
									.getEntityStream());
							return Response.ok(temp.removeById(param)).build();
						}
					});

			resource = resourceBuilder.build();
			resources.add(resource);
		}
	}

	public JSONObject convertFromInputStream(InputStream is) {
		StringBuilder sb = new StringBuilder();
		int i;
		char c;
		try {
			while ((i = is.read()) != -1) {
				c = (char) i;
				if (c != ' ' || c != '\n') sb.append(c);
			}
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(sb.toString());
			return json;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;
	}
}
