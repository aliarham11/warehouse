package ai.kalimat.warehouse.models;


public class MainJson {

	private String component;
	
	private String method;
	
	private Integer[] args;

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Integer[] getArgs() {
		return args;
	}

	public void setArgs(Integer[] args) {
		this.args = args;
	}
	
	
	
}
