package ai.kalimat.warehouse.models;

import java.util.List;

public class ComponentJson {
	
	private String className;
	
	private List<String> dependencies;
	

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public List<String> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<String> dependencies) {
		this.dependencies = dependencies;
	}
	
	
	
}
