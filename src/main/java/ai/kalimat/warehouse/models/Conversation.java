package ai.kalimat.warehouse.models;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

public class Conversation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5260998811460468754L;
	
	private Integer id;
	
	private Integer userId;
	
	private String direction;
	
	private String message;
	
	private Integer timestamp;
	
	@JsonIgnore
	private Boolean isDeleted;

	public Conversation(){}
	
	public Conversation(Integer id, Integer userId, String direction,
			String message, Integer timestamp, Boolean isDeleted) {
		super();
		this.id = id;
		this.userId = userId;
		this.direction = direction;
		this.message = message;
		this.timestamp = timestamp;
		this.isDeleted = isDeleted;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
	
	
	
	
}
