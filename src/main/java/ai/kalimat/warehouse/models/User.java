package ai.kalimat.warehouse.models;
import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8728060629939206576L;

	private Integer id;
	
	private String name;
	
	private String gender;
	
	private String city;
	
	private String phone;
	
	private String email;
	
	@JsonIgnore
	private Boolean isDeleted;
	
	public User(){}

	public User(Integer id, String name, String gender, String city,
			String phone, String email, Boolean isDeleted) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.city = city;
		this.phone = phone;
		this.email = email;
		this.isDeleted = isDeleted;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
	
	

}
