package ai.kalimat.warehouse.containers;

import ai.kalimat.warehouse.services.ConversationService;
import ai.kalimat.warehouse.services.UserService;

public final class ServiceContainer {
	
	private static UserService userService;
	
	private static ConversationService conversationService;
	
	public ServiceContainer(UserService userService, ConversationService conversationService){
		ServiceContainer.userService = userService;
		ServiceContainer.conversationService = conversationService;
	}
	
	public static UserService getUserServiceInstance(){
		return ServiceContainer.userService;
	}
	
	public static ConversationService getConversationServiceInstance(){
		return ServiceContainer.conversationService;
	}
	
}
