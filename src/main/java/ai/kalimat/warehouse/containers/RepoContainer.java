package ai.kalimat.warehouse.containers;

import ai.kalimat.warehouse.repos.ConversationRepo;
import ai.kalimat.warehouse.repos.UserRepo;

public final class RepoContainer {
	
	private static UserRepo userRepo;
	
	private static ConversationRepo conversationRepo;
	
	public RepoContainer(UserRepo userRepo, ConversationRepo conversationRepo){
		RepoContainer.userRepo = userRepo;
		RepoContainer.conversationRepo = conversationRepo;
	}
	
	public static UserRepo getUserRepoInstance(){
		return userRepo;
	}
	
	public static ConversationRepo getConversationRepoInstance(){
		return conversationRepo;
	}
}
