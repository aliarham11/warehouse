package ai.kalimat.warehouse.containers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class ObjectContainer {
	
	private static final Map<String, Object> OBJECT_CONTAINER = new ConcurrentHashMap<String, Object>();
	
	public static Object getObject(String key){
		return OBJECT_CONTAINER.get(key);
	}
	
	public static void storeObject(String key, Object obj){
		OBJECT_CONTAINER.put(key, obj);
	}
}
