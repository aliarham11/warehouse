package ai.kalimat.warehouse.storages;

import java.util.List;

public interface Store<T> {
	
	T getById(String entityName, Integer id);
	
	List<T> getAll(String entityName);
	
	T saveOrUpdate(String entityName, T data, Integer id);
	
	String delete(String entityName, T data, Integer id);

}
