package ai.kalimat.warehouse.storages;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.spy.memcached.MemcachedClient;

public class MemStore<T> implements Store<T>{
	
	public enum CacheExpiryEnum {
		MINUTE(60),
		FIVE_MINUTES(300),
		FIFTEEN_MINUTES(900),
		THIRTY_MINUTES(1800),
		HOUR(3600),
		DAY(86400),
		WEEK(604800),
		THIRTY_DAYS(2592000);
		
		Integer value;
		
		private CacheExpiryEnum(Integer expiry) {
			this.value = expiry;
		}
		
		public Integer getValue() {
			return value;
		}
	}
	
	private MemcachedClient cache;
	
	public MemStore(){
		Properties prop = new Properties();
		InputStream config = null;
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		cache = null;
		try {
			config = loader.getResourceAsStream("config.properties");
			prop.load(config);
			String host = prop.getProperty("cache.host");
			Integer port = Integer.parseInt(prop.getProperty("cache.port"));
			cache = new MemcachedClient(new InetSocketAddress(host, port));
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	public T getById(String entityName, Integer id) {
		System.out.println("Get from memory");
		String key = String.format("%s_%s", entityName, id.toString());
		
		return (T)cache.get(key);
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll(String entityName) {
		System.out.println("Get all from MemStore");
		String keyAll = entityName;
		
		return (List<T>)cache.get(keyAll);
	}

	@SuppressWarnings("unchecked")
	public T saveOrUpdate(String entityName, T data, Integer id) {
		System.out.println("Save from MemStore");
		String key = String.format("%s_%s", entityName, id.toString()); 
		String keyAll = entityName;
		List<T> allData = (List<T>)cache.get(keyAll);
		
		if(allData == null){
			allData = new ArrayList<T>();	
		}
		
		if(cache.get(key) != null){
			cache.replace(key, CacheExpiryEnum.DAY.getValue(), data);
			if(id <= allData.size()) allData.set(id - 1, data);
		} else {
			cache.set(key, CacheExpiryEnum.DAY.getValue(), data);
			allData.add(data);
		}
		
		cache.set(keyAll, CacheExpiryEnum.DAY.getValue(), allData);
		
		return data;
	}

	@SuppressWarnings("unchecked")
	public String delete(String entityName, T data, Integer id) {
		System.out.println("Delete from MemStore");
		String key = String.format("%s_%s", entityName, id.toString());
		String keyAll = entityName;
		List<T> allData = (List<T>)cache.get(keyAll);
		String message = "";
		
		allData.set(id - 1, data);
		cache.replace(key, CacheExpiryEnum.DAY.getValue(), data);
		cache.set(keyAll, CacheExpiryEnum.DAY.getValue(), allData);
		message = "Success";
		
		return message;
	}
}
