package ai.kalimat.warehouse.storages;

import java.util.List;

public class DBStore<T> implements Store<T> {

	public T getById(String entityName, Integer id) {
		System.out.println("get by id from DBStore");
		return null;
	}

	public List<T> getAll(String entityName) {
		System.out.println("get all from DBStore");
		return null;
	}

	public T saveOrUpdate(String entityName, T data, Integer id) {
		System.out.println("Save from DBStore");
		return null;
	}

	public String delete(String entityName, T data, Integer id) {
		System.out.println("delete from DBStore");
		return null;
	}

}
