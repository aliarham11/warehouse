package ai.kalimat.warehouse.injectors;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import ai.kalimat.warehouse.containers.ObjectContainer;
import ai.kalimat.warehouse.models.ComponentJson;
import ai.kalimat.warehouse.models.MainJson;

public class Injector {

	private Map<String, ComponentJson> componentMapper;
	
	private MainJson mainJson;
	
	private ObjectMapper mapper;
	
	@SuppressWarnings("rawtypes")
	private Class[] _class;
	
	private Object[] _object;
	
	public Injector(){
		mapper = new ObjectMapper();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void loadConfig(String fileName){
		byte[] json = null;
		
		try {
			
			Map<String, Object> objectJsonMapper = mapper.readValue(
					new File(fileName), 
					new TypeReference<Map<String, Object>>(){}
				);
			json = mapper.writeValueAsBytes(objectJsonMapper.get("components"));
			componentMapper = mapper.readValue(
					json,
					new TypeReference<Map<String, ComponentJson>>(){}
				);
			componentMapper.forEach((key,val) -> {
				ComponentJson objectJsonInstance = val;
				Integer dependencySize = objectJsonInstance.getDependencies().size();
				try {
					if(dependencySize > 0){
						_class = new Class[dependencySize];
						_object = new Object[dependencySize];
						for(int i = 0; i < dependencySize; i++){
							String entityName = objectJsonInstance.getDependencies().get(i);
							if(ObjectContainer.getObject(entityName) != null){
								if(ObjectContainer.getObject(entityName).getClass().getInterfaces().length == 1){
									_class[i] = ObjectContainer.getObject(entityName).getClass().getInterfaces()[0];
								} else {
									_class[i] = ObjectContainer.getObject(entityName).getClass();
								}
								
								_object[i] = ObjectContainer.getObject(entityName);
							} else {
								_class[i] = entityName.getClass();
								_object[i] = entityName;
							}
							
						}
						
						Class tempClass = Class.forName(objectJsonInstance.getClassName());
						Constructor con = tempClass.getConstructor(_class);
						ObjectContainer.storeObject(key, con.newInstance(_object));
						
					} else {
						ObjectContainer.storeObject(key, Class.forName(objectJsonInstance.getClassName()).newInstance());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			
			startApp(mapper.writeValueAsBytes(objectJsonMapper.get("main")));
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
	}
	
	public void startApp(byte[] json) {
		try {
			mainJson = mapper.readValue(json, MainJson.class);
			Object mainObject = ObjectContainer.getObject(mainJson.getComponent());
			Method mainMethod = mainObject.getClass().getMethod(mainJson.getMethod(), new Class[]{Integer.class});
			Object[] obj = new Object[mainJson.getArgs().length];
			
			for(int i = 0; i < mainJson.getArgs().length; i++){
				obj[i] = mainJson.getArgs()[i];
			}
			mainMethod.invoke(mainObject, obj);
			
		} catch (IOException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	
}