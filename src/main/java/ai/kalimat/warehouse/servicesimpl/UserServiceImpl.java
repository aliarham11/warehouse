package ai.kalimat.warehouse.servicesimpl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ai.kalimat.warehouse.containers.RepoContainer;
import ai.kalimat.warehouse.models.User;
import ai.kalimat.warehouse.repos.UserRepo;
import ai.kalimat.warehouse.services.UserService;

public class UserServiceImpl implements UserService {

	private UserRepo userRepo = RepoContainer.getUserRepoInstance();
	
	public Map<String, User> storeData(User user) {
		Map<String, User> result = new ConcurrentHashMap<String, User>();
		result.put("result", userRepo.save(user));
		return result; 
	}

	public Map<String, User> fetchById(Integer id) {
		Map<String, User> result = new ConcurrentHashMap<String, User>();
		User userResult = userRepo.getById(id);
		if (userResult == null) {
			userResult = userRepo.getDefault();
		}
		result.put("result", userResult);
		return result;
	}

	public Map<String, List<User>> fetchAll() {
		Map<String, List<User>> result = new ConcurrentHashMap<String, List<User>>();
		List<User> userResults = userRepo.getAll();
		if (userResults == null){
			userResults = Arrays.asList(userRepo.getDefault());
		}
		result.put("result", userResults);
		return result;
	}

	public Map<String, String> removeById(User user) {
		Map<String, String> result = new ConcurrentHashMap<String, String>();
		result.put("result", userRepo.removeById(user));
		return result;
	}

}
