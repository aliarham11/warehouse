package ai.kalimat.warehouse.servicesimpl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ai.kalimat.warehouse.containers.RepoContainer;
import ai.kalimat.warehouse.models.Conversation;
import ai.kalimat.warehouse.repos.ConversationRepo;
import ai.kalimat.warehouse.services.ConversationService;

public class ConversationServiceImpl implements ConversationService {
	
	private ConversationRepo conversationRepo = RepoContainer.getConversationRepoInstance();
	
	public Map<String, Conversation> storeData(Conversation conversation) {
		Map<String, Conversation> result = new ConcurrentHashMap<String, Conversation>();
		result.put("result", conversationRepo.save(conversation));
		return result; 
	}

	public Map<String, Conversation> fetchById(Integer id) {
		Map<String, Conversation> result = new ConcurrentHashMap<String, Conversation>();
		Conversation conversationResult = conversationRepo.getById(id);
		if (conversationResult == null) {
			conversationResult = conversationRepo.getDefault();
		}
		result.put("result", conversationResult);
		return result;
	}

	public Map<String, List<Conversation>> fetchAll() {
		Map<String, List<Conversation>> result = new ConcurrentHashMap<String, List<Conversation>>();
		List<Conversation> conversationResults = conversationRepo.getAll();
		if (conversationResults == null){
			conversationResults = Arrays.asList(conversationRepo.getDefault());
		}
		result.put("result", conversationResults);
		return result;
	}

	public Map<String, String> removeById(Conversation conversation) {
		Map<String, String> result = new ConcurrentHashMap<String, String>();
		result.put("result", conversationRepo.removeById(conversation));
		return result;
	}


}
