# WAREHOUSE - kata.ai interview challenge #

# Overview #
This repo is for kata.ai interview challenge.  There are guidelines for setup this project on your machine, and answers for kata.ai interview challenge question,  check it out XD !!

# How do i setup this project in my machine ? #
Well, for the sake of avoiding "in my stuff works" problem, please follow this guidelines carefully, its quite simple though.

1. Make sure you have maven and Java 8 installed on your machine
2. Make sure you have memcached installed too
3. Clone this project
4. Configure memcached host and port in config.properties (src/main/resources)

.. and you are ready to go

# How do i run the main program ? #
1. Make sure memcached has ben started
2. run : mvn clean install
3. run : mvn exec:java -Dexec.mainClass="ai.kalimat.warehouse.main.AppInitialize" -Dexec.args="your_json_args_file.json"

for example : mvn exec:java -Dexec.mainClass="ai.kalimat.warehouse.main.AppInitialize" -Dexec.args="parameterGenericJson.json"

.. if application is successfully started, you will see this on you terminal
![Screen Shot 2017-01-09 at 20.26.25.png](https://bitbucket.org/repo/4eoEKX/images/2301194729-Screen%20Shot%202017-01-09%20at%2020.26.25.png)

.. and you can access it via browser (url path and port are based on your json configuration that passed into app)
![Screen Shot 2017-01-09 at 20.36.41.png](https://bitbucket.org/repo/4eoEKX/images/3804736850-Screen%20Shot%202017-01-09%20at%2020.36.41.png)

NOTES : 

* if you have already run mvn clean install, you dont need to do it again if you want to restart the main app.

* if you using GenericRepo for repo, path are begin with _generic/your_repo_name_



# Questions and Answers for kata.ai interview challenge #

> # Initial Prototype #

-- QUESTIONS --

1. Why the type of store is Store and not Memstore ?

2. What design patterns do you recognize here?

-- ANSWERS --

1. Because if we want to change our store procedure into, for example DBStore, its quite painful if we have to change all declaration from MemStore to DBStore all over the code in our current application.  So, to avoid that, we just declare one interface called Store that later can be implemented by MemStore or other store procedure, so that if we want to change our store procedure, we just have to change the instantiation. 
![Screen Shot 2017-01-07 at 17.05.28.png](https://bitbucket.org/repo/4eoEKX/images/452402479-Screen%20Shot%202017-01-07%20at%2017.05.28.png)

2. The design pattern that i recognize here is Factory Method Design Pattern


> # Injectable Component #

-- QUESTIONS --

1. What is the advantage of having initialization in configuration file?

2. What is this pattern called?

3. If you created a DBStore with path ai.kalimat.stores.DBStore. And you have already this
class compiled:
a. What do you have to do, to swap the MemStore with DBStore?
b. Do you need to recompile?

4. Define steps to be done if you want to have the configuration file definable in environment variable like this : 

![Screen Shot 2017-01-09 at 20.41.29.png](https://bitbucket.org/repo/4eoEKX/images/1694456307-Screen%20Shot%202017-01-09%20at%2020.41.29.png)

-- ANSWERS --

1. We can dynamically change the class initialization based on configuration file, and we don't need to recompile the application if we want to change the class's initialization, just change the config file, and restart the apps, or run the apps again with another config file.

2. It's a Factory Design Pattern

3. a. You just have to change object initialization in the configuration file from MemStore to DBStore

    * Before : ![Screen Shot 2017-01-09 at 21.25.31.png](https://bitbucket.org/repo/4eoEKX/images/2167866163-Screen%20Shot%202017-01-09%20at%2021.25.31.png)

    * After : ![Screen Shot 2017-01-09 at 21.25.41.png](https://bitbucket.org/repo/4eoEKX/images/4259050612-Screen%20Shot%202017-01-09%20at%2021.25.41.png)

    b. Nope, just restart the apps with new config file or run the apps again with another port defined in the new config file

4.  To get concat JSON string with env param, simple echo command will do the trick.  In the code, i have to check the params if it's a JSON string param or fileName param, if it's a JSON string param then no need to read a file, direct pass it to Injector class (but i haven't implemented it on app yet).

    ![Screen Shot 2017-01-10 at 06.05.45.png](https://bitbucket.org/repo/4eoEKX/images/2684843-Screen%20Shot%202017-01-10%20at%2006.05.45.png)

> # Generic Repo #

-- QUESTIONS --

1. What is the benefit of this change?

2. What should I change if I want to add new path /tokens with following fields: string
token; int expire;

3. Do I have to recompile?

-- ANSWERS --

1. It's super dynamic than iteration number 2, not only can dynamically initialize the class, it can dynamically initialize the resource too.  So, if you have another resource, you just have to initialize the repos and the root path in the config file without changing the main code of the application.

2. As i say above, just add the repos and the root path in the config file, so the config file become like this :

    ![Screen Shot 2017-01-09 at 22.27.09.png](https://bitbucket.org/repo/4eoEKX/images/2645406469-Screen%20Shot%202017-01-09%20at%2022.27.09.png)

3. Nope, just restart the apps with new config file or run the apps again with another port defined in the new config file